<?php

namespace Drupal\disable_page_slash_node\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\SiteInformationForm;

/**
 * Configure site information settings for this site.
 *
 * @internal
 */
class DisablePageNodeSiteInformationForm extends SiteInformationForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the system.site configuration.
    $site_config = $this->config('system.site');

    // Get the original form from the class we are extending.
    $form = parent::buildForm($form, $form_state);

    $form['front_page']['site_disable_page_node'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Retain <a href="@url">/node</a> as an active url?', [
        '@url' => $this->aliasManager->getAliasByPath('/node'),
      ]),
      '#default_value' => $site_config->get('site_disable_page_node') ?? TRUE,
      '#states' => [
        'invisible' => [
          ':input[name=site_frontpage]' => [
            'value' => '/node',
          ],
        ],
      ],
    ];

    $form['front_page']['site_disable_page_node_404'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Return 404 on <a href="@url">/node</a>', [
        '@url' => $this->aliasManager->getAliasByPath('/node'),
      ]),
      '#description' => $this->t('Rather than redirecting to front page.'),
      '#default_value' => $site_config->get('site_disable_page_node_404') ?? FALSE,
      '#states' => [
        'invisible' => [
          [
            ':input[name=site_frontpage]' => [
              'value' => '/node',
            ],
          ],
          'or',
          [
            ':input[name=site_disable_page_node]' => [
              'checked' => TRUE,
            ],
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $siteFrontPage = $form_state->getValue('site_frontpage');
    $disablePageNode = $form_state->getValue('site_disable_page_node');
    $return404 = $form_state->getValue('site_disable_page_node_404');

    if ($siteFrontPage === '/node') {
      $disablePageNode = TRUE;
      $return404 = FALSE;
    }
    elseif ($disablePageNode === 1) {
      $return404 = FALSE;
    }

    $this->config('system.site')
      ->set('site_disable_page_node', $disablePageNode)
      ->save();

    $this->config('system.site')
      ->set('site_disable_page_node_404', $return404)
      ->save();

    parent::submitForm($form, $form_state);

  }

}
